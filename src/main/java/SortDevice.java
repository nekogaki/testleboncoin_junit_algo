import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SortDevice {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Device device1 = new Device("3.0.1");
		Device device2 = new Device("2.1.2");
		Device device3 = new Device("3.0.1.3");

		List<Device> deviceList = new ArrayList<Device>();
		List<Device> deviceSortedList = new ArrayList<Device>();
		
		deviceList.add(device1);
		deviceList.add(device2);
		deviceList.add(device3);

		deviceSortedList = Algo.sort(deviceList);
		System.out.println(deviceSortedList.get(0).getVersion());
		System.out.println(deviceSortedList.get(1).getVersion());
		
	}

}
