import java.util.ArrayList;
import java.util.List;

public class Algo {

	/**
	 * Get a devices List and alphabetically sort it.
	 * @param devices<List>
	 * @return sorted devices<List>
	 */
	public static List<Device> sort(List<Device> devices){
		
		ArrayList<String> listVersion = new ArrayList<String>();
		// Get version of each devices
		for(int i=0; i< devices.size() ; i++){
			listVersion.add(devices.get(i).getVersion());
		}
	
		Integer i;
		Integer j;
		Integer min;
		Device temp;
		// Iterate over each version
		for(i = 0; i < listVersion.size(); i++) {
			min = i;
			// And iterate again
			for(j = i; j < listVersion.size(); j++) {
				//If second for actual iteration is lexicographically less than first for actual iteration
				if(listVersion.get(min).compareTo(listVersion.get(j)) > 0) {					
					min = j;
				}
			}
			
			//Then, permut devices objects
			temp = devices.get(i);
			devices.set(i, devices.get(min));
			devices.set(min, temp);			
		}
		
		return devices;		
	}
	
}
