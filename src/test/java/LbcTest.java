import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import junit.framework.Assert;

public class LbcTest {
	
	@Test
	public void shouldFirstTest() {
		
		Device device1 = new Device("3.0.1");
		Device device2 = new Device("2.1.2");
		Device device3 = new Device("3.0.1.3");

		List<Device> deviceList = new ArrayList<Device>();
		List<Device> deviceSortedList = new ArrayList<Device>();
		
		deviceList.add(device1);
		deviceList.add(device2);
		deviceList.add(device3);

		deviceSortedList = Algo.sort(deviceList);
		
		Assert.assertEquals(deviceSortedList.get(0).getVersion(), device2.getVersion());
		Assert.assertEquals(deviceSortedList.get(1).getVersion(), device1.getVersion());
		Assert.assertEquals(deviceSortedList.get(2).getVersion(), device3.getVersion());
	}
	
}
